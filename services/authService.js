const config = require('../config.json')
import axios from 'axios'

export class AuthService {
  constructor() {
    this.urlAPI = config.url_API
  }

  authUser = async (email, password) => {
    try {
      const response = await axios.post(`${this.urlAPI}/auth`, {
        email: email,
        password: password
      })
      console.log(response.data)
      return response.data
    } catch (e) {
      // load mock if request fail in order to debug on private
      console.log(e)
      return { error: true, message: 'Bad password or email' }
    }
  }
}
