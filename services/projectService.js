const config = require('../config.json')
import axios from 'axios'
import { AsyncStorage } from 'react-native'

export class ProjectService {
  constructor() {
    this.urlAPI = config.url_API
  }

  fetchProjects = async () => {
    try {
      const jwt = await AsyncStorage.getItem('@jwt')
      const response = await axios.get(`${this.urlAPI}/project`, {
        params: {},
        headers: { Authorization: `Bearer ${jwt}` }
      })
      return response.data.data
    } catch (e) {
      console.log(e)
      return null
    }
  }

  uploadProject = async project => {
    try {
      const { category, title, description, isPrivate, date } = project
      const jwt = await AsyncStorage.getItem('@jwt')
      console.log(jwt)
      const response = await axios.post(
        `${this.urlAPI}/project`,
        {
          title,
          start_date: date,
          nb_attendee_min: 100,
          nb_attendee_max: 200,
          description,
          isPrivate,
          category: 1
        },
        {
          headers: {
            Authorization: `Bearer ${jwt}`
          }
        }
      )
      return response.data.data
    } catch (e) {
      console.log(e)
      return null
    }
  }
}
