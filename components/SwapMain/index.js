import React, { Component } from 'react'
import Swiper from 'react-native-deck-swiper'
import { ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import { Platform } from 'react-native'
import { ProjectService } from '../../services/projectService'
import ListProject from './components/ListProject'
import mainBg from '../../assets/main_bg.png'
import calendar from '../../assets/calendar.png'
import gugusse from '../../assets/gugusse.png'

export default class Cards extends Component {
  constructor(props) {
    super(props)
    this.projectService = new ProjectService()
    this.state = {
      cards: null,
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      curIndex: 0
    }
  }

  componentWillMount = async () => {
    const projects = await this.projectService.fetchProjects()
    this.setState({
      cards: projects
    })
  }

  renderCard = (card, index) => {
    const category = getCate(card.category)
    return (
      <View style={styles.card}>
        <View
          style={{
            backgroundColor: '#21304D',
            borderRadius: 10,
            alignSelf: 'flex-start',
            // flex: 1,
            width: '100%'
          }}
        >
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 16,
              padding: 3
            }}
          >
            Interne à l'école
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              textAlign: 'center',
              marginTop: '5%'
            }}
          >
            {card.title.toUpperCase()}
          </Text>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                height: 10,
                width: 10,
                borderRadius: 10,
                backgroundColor: `#${intToRGB(hashCode(category))}`,
                marginRight: 5
              }}
            ></View>
            <Text
              style={{
                fontSize: 14,
                textAlign: 'center'
              }}
            >
              {category}
            </Text>
          </View>
          <Text style={{ fontSize: 16, textAlign: 'justify', margin: '10%' }}>
            {card.description}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            paddingTop: '5%',
            justifyContent: 'flex-start',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end'
          }}
        >
          <Image source={gugusse}></Image>
          <Text>{`Entre ${card.nb_attendee_min} et ${card.nb_attendee_max} personnes`}</Text>
        </View>
        <View
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: '5%'
          }}
        >
          <Image source={calendar}></Image>
          <Text>{card.start_date}</Text>
        </View>
      </View>
    )
  }

  onSwiped = () => {
    const { curIndex } = this.state
    this.setState({ curIndex: curIndex + 1 })
  }

  onSwipedAllCards = () => {
    this.setState({
      swipedAllCards: true
    })
  }

  swipeLeft = () => {
    this.swiper.swipeLeft()
  }

  render() {
    const { cards, swipedAllCards, curIndex } = this.state
    if (swipedAllCards || !cards) {
      return (
        <ImageBackground
          source={mainBg}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={styles.done}>
            <ListProject projects={cards} />
          </View>
        </ImageBackground>
      )
    }
    return (
      <View style={styles.container}>
        <ImageBackground
          source={mainBg}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={{ flex: 1 }}>
            <Text>Swipez gauche / droite</Text>
          </View>
          <Swiper
            backgroundColor="transparent"
            ref={swiper => {
              this.swiper = swiper
            }}
            onSwipedLeft={() => this.onSwiped('left')}
            onSwipedRight={() => this.onSwiped('right')}
            useViewOverflow={Platform.OS === 'ios'}
            onTapCard={this.swipeLeft}
            cards={this.state.cards}
            verticalSwipe={false}
            cardIndex={this.state.cardIndex}
            cardVerticalMargin={80}
            renderCard={this.renderCard}
            onSwipedAll={this.onSwipedAllCards}
            stackSize={cards.length > 5 ? 5 : cards.length}
            stackSeparation={15}
            overlayLabels={{
              left: {
                title: '✖️',
                style: {
                  label: {
                    backgroundColor: 'transparent',
                    fontSize: 30,
                    color: 'white'
                  },
                  wrapper: {
                    flexDirection: 'column',
                    alignItems: 'flex-end',
                    justifyContent: 'flex-start',
                    marginTop: 20,
                    marginLeft: -10
                  }
                }
              },
              right: {
                title: '❤️',
                style: {
                  label: {
                    fontSize: 30,
                    backgroundColor: 'transparent',
                    color: 'white'
                  },
                  wrapper: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    marginTop: 20,
                    marginLeft: 10
                  }
                }
              }
            }}
            animateOverlayLabelsOpacity
            animateCardOpacity
            swipeBackCard
          ></Swiper>
          <View
            style={{
              height: '7%',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}>
              {`${curIndex + 1} / ${cards.length}`}
            </Text>
            <Text style={{ color: 'white', fontSize: 14 }}>
              Swipez à gauche ou à droite
            </Text>
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#41204D'
  },
  card: {
    flex: 1,
    // flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white'
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent'
  },
  done: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

function hashCode(str) {
  // java String#hashCode
  var hash = 0
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  return hash
}

function intToRGB(i) {
  var c = (i & 0x00ffffff).toString(16).toUpperCase()

  return '00000'.substring(0, 6 - c.length) + c
}

function getCate(id) {
  switch (id) {
    case 1:
      return 'Soirée'
    case 2:
      return 'LAN'
    case 3:
      return 'Jeux vidéos'
    case 4:
      return 'Formation'
    default:
      return 'Soirée'
  }
}
