import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableHighlight
} from 'react-native'
import topBg from '../../../../assets/top_head_ban.png'
import picture from '../../../../assets/picture.png'
import arrow from '../../../../assets/arrow.png'
import { Actions } from 'react-native-router-flux'

export default class ListProject extends Component {
  constructor(props) {
    super(props)
    const { projects } = props
    this.state = { projects }
  }

  goToCreate = () => {
    Actions.createProject()
  }

  renderProjects = () => {
    const { projects } = this.state
    const res = []
    if (!projects) {
      return <View></View>
    }
    projects.map(project => {
      const category = getCate(project.category)
      res.push(
        <View
          style={{
            shadowColor: 'white',
            padding: 30,
            borderRadius: 10,
            borderColor: `#${intToRGB(hashCode(category))}`,
            margin: 5,
            borderWidth: 1
          }}
        >
          <Text
            style={{
              fontSize: 12,
              color: '#21304D',
              marginTop: -20,
              marginBottom: 10,
              alignSelf: 'flex-end'
            }}
          >
            {category}
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              color: '#21304D',
              marginBottom: 10,
              marginTop: -10
            }}
          >
            {project.title.toUpperCase()}
          </Text>
          <Text style={{ fontSize: 16, color: '#21304D' }}>
            {`Evenement de ${project.nb_attendee_max} personnes`}
          </Text>
          <Text style={{ fontSize: 16, color: '#21304D' }}>
            {project.start_date}
          </Text>
        </View>
      )
    })
    return res
  }

  render() {
    const { projects } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={StyleSheet.absoluteFill} source={topBg} />
          <Text style={styles.headerText}>BDE</Text>
          <Text style={styles.headerSubText}>Blue Digital Eagles</Text>
          <TouchableHighlight
            style={{ position: 'absolute', top: 50, right: 25 }}
            underlayColor={'#21304D'}
            onPress={() => Actions.auth()}
          >
            <Image source={picture}></Image>
          </TouchableHighlight>
        </View>
        <TouchableHighlight
          onPress={() => this.goToCreate()}
          style={styles.addEvent}
          underlayColor="white"
        >
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <Text style={{ color: 'white', fontSize: 16, marginRight: 20 }}>
              Proposer un évènement
            </Text>
            <Image source={arrow}></Image>
          </View>
        </TouchableHighlight>
        <Text
          style={{
            alignSelf: 'flex-start',
            marginLeft: '10%',
            marginTop: '5%',
            marginBottom: '3%'
          }}
        >
          EVENEMENTS A VENIR
        </Text>
        <ScrollView style={styles.scrollView}>
          {this.renderProjects()}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  addEvent: {
    marginTop: -125,
    marginLeft: -100,
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#21304D',
    borderRadius: 10,
    alignItems: 'flex-start'
  },
  container: {
    // flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white'
  },

  headerText: {
    color: 'white',
    fontSize: 36,
    position: 'absolute',
    fontWeight: 'bold',
    top: '15%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  headerSubText: {
    color: 'white',
    fontSize: 16,
    position: 'absolute',
    top: '27%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  content: {
    // flex: 1,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

  header: {
    margin: -2,
    height: '45%',
    width: '101%'
  },

  input: {
    width: '70%',
    marginTop: '1%',
    marginBottom: '2%',
    borderRadius: 6,
    borderWidth: 1,
    padding: 10,
    borderColor: '#111E2F'
  },

  footer: {
    margin: 50,
    width: '50%',
    borderRadius: 10,
    borderWidth: 1.5,
    padding: 20,
    borderColor: '#21304D',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

function hashCode(str) {
  // java String#hashCode
  var hash = 0
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  return hash
}

function intToRGB(i) {
  var c = (i & 0x00ffffff).toString(16).toUpperCase()

  return '00000'.substring(0, 6 - c.length) + c
}

function getCate(id) {
  switch (id) {
    case 1:
      return 'Soirée'
    case 2:
      return 'LAN'
    case 3:
      return 'Jeux vidéos'
    case 4:
      return 'Formation'
    default:
      return 'Soirée'
  }
}
