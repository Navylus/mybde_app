import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  Picker,
  ScrollView,
  TouchableHighlight
} from 'react-native'
import { CheckBox } from 'react-native-elements'
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import topBg from '../../../../assets/top_head_ban.png'
import picture from '../../../../assets/picture.png'
import gugusse from '../../../../assets/gugusse.png'
import DatePicker from 'react-native-datepicker'
import { Actions } from 'react-native-router-flux'
import { ProjectService } from '../../../../services/projectService'

export default class CreateProject extends Component {
  constructor(props) {
    super(props)
    this.projectService = new ProjectService()
    this.state = {
      category: 'Soirée',
      title: '',
      description: '',
      isPrivate: 0,
      date: '21/02/2020'
    }
  }

  upladProject = async () => {
    const { category, title, description, isPrivate, date } = this.state
    await this.projectService.uploadProject({
      category,
      title,
      description,
      isPrivate,
      date
    })
    Actions.cards()
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={StyleSheet.absoluteFill} source={topBg} />
          <Text style={styles.headerText}>BDE</Text>
          <Text style={styles.headerSubText}>Blue Digital Eagles</Text>
          <Image
            style={{ position: 'absolute', top: 50, right: 25 }}
            source={picture}
          ></Image>
        </View>
        <ScrollView style={{ width: '80%', marginTop: '-20%' }}>
          <Text
            style={{
              alignSelf: 'flex-start',
              marginLeft: '-10%',
              marginTop: '0%',
              marginBottom: '3%'
            }}
          >
            PROPOSITION D'EVENEMENT
          </Text>
          <View style={{ marginTop: '5%' }}>
            <Text>Titre</Text>
            <TextInput
              style={{
                height: 40,
                padding: 10,
                margin: 3,
                borderColor: '#111E2F',
                borderWidth: 1,
                borderRadius: 6
              }}
              onChangeText={itemValue => this.setState({ title: itemValue })}
              value={this.state.title}
            />
          </View>
          <View style={{ marginTop: 20 }}>
            <Text>Catégorie</Text>
            <View
              style={{
                borderColor: '#21304D',
                borderWidth: 1,
                borderRadius: 6,
                marginTop: 10
              }}
            >
              <Picker
                selectedValue={this.state.category}
                style={{
                  height: 50,
                  width: 150,
                  borderRadius: 6,
                  borderWidth: 5,
                  borderColor: '#111E2F'
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ category: itemValue })
                }
              >
                <Picker.Item label="Soirée" value="1" />
                <Picker.Item label="LAN" value="2" />
                <Picker.Item label="Jeux vidéos" value="3" />
                <Picker.Item label="Formation" value="4" />
              </Picker>
            </View>
          </View>
          <View style={{ marginTop: '10%' }}>
            <Text>Description</Text>
            <TextInput
              style={{
                height: 150,
                padding: 10,
                margin: 3,
                borderColor: '#111E2F',
                borderWidth: 1,
                borderRadius: 6
              }}
              textAlignVertical="top"
              multiline
              maxLength={80}
              onChangeText={itemValue =>
                this.setState({ description: itemValue })
              }
              value={this.state.description}
            />
          </View>
          <View
            style={{ display: 'flex', flexDirection: 'row', marginTop: 40 }}
          >
            <Image source={gugusse} style={{ marginRight: '5%' }}></Image>
            <MultiSlider
              values={[10, 1000]}
              sliderLength={200}
              customMarkerLeft={() => <Text>TEE</Text>}
              enableLabel={e => alert(e)}
              min={10}
              max={500}
              step={10}
              enabledTwo={true}
            />
          </View>
          <View>
            <CheckBox
              center
              title="Privé à l'école"
              checked={this.state.isPrivate}
              checkedColor="#21304D"
              onPress={() =>
                this.setState({ isPrivate: !this.state.isPrivate })
              }
            />
          </View>
          <View>
            <DatePicker
              style={{ width: 200, marginTop: 20 }}
              date={this.state.date}
              mode="date"
              placeholder="Choisir une date"
              format="DD/MM/YYYY"
              minDate="01/05/2019"
              maxDate="01/05/2030"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  borderRadius: 6,
                  borderColor: '#21304D',
                  marginLeft: 36
                }
              }}
              onDateChange={date => {
                this.setState({ date: date })
              }}
            />
          </View>
          <TouchableHighlight
            style={styles.footer}
            underlayColor={'#21304D'}
            onPress={() => this.upladProject()}
          >
            <Text>Valider</Text>
          </TouchableHighlight>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  addEvent: {
    marginTop: -125,
    marginLeft: -100,
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#21304D',
    borderRadius: 10,
    alignItems: 'flex-start'
  },
  container: {
    // flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white'
  },

  headerText: {
    color: 'white',
    fontSize: 36,
    position: 'absolute',
    fontWeight: 'bold',
    top: '15%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  headerSubText: {
    color: 'white',
    fontSize: 16,
    position: 'absolute',
    top: '27%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  content: {
    // flex: 1,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

  header: {
    margin: -2,
    height: '45%',
    width: '101%'
  },

  input: {
    width: '70%',
    marginTop: '1%',
    marginBottom: '2%',
    borderRadius: 6,
    borderWidth: 1,
    padding: 10,
    borderColor: '#111E2F'
  },

  footer: {
    margin: 50,
    width: '50%',
    borderRadius: 10,
    borderWidth: 1.5,
    padding: 20,
    borderColor: '#21304D',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
