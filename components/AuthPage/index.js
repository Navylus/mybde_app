import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  AsyncStorage
} from 'react-native'
import topBg from '../../assets/top_head_ban.png'
import { AuthService } from '../../services/authService'
import { Actions } from 'react-native-router-flux'

export default class Auth extends Component {
  constructor(props) {
    super(props)
    this.authService = new AuthService()
    this.state = {
      valuePwd: '',
      valueEmail: '',
      error: ''
    }
  }

  goToCards = () => {
    Actions.cards()
  }

  connect = async (valueEmail, valuePwd) => {
    try {
      res = await this.authService.authUser(valueEmail, valuePwd)
      if (res.error || res.code === 400) {
        this.setState({ error: res.message })
      } else {
        await AsyncStorage.setItem('@jwt', res.jwt)
        this.goToCards()
        this.setState({ error: 'logged in.' })
      }
    } catch (e) {
      console.log(e)
    }
  }

  changeEmail = email => {
    this.setState({ valueEmail: email })
  }

  changePwd = pwd => {
    this.setState({ valuePwd: pwd })
  }

  render() {
    const { valueEmail, valuePwd, error } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={StyleSheet.absoluteFill} source={topBg} />
          <Text style={styles.headerText}>BDE</Text>
          <Text style={styles.headerSubText}>Blue Digital Eagles</Text>
        </View>
        <View style={styles.content}>
          <Text
            style={{
              alignSelf: 'flex-start',
              marginLeft: '10%',
              marginTop: '-10%',
              marginBottom: '3%'
            }}
          >
            CONNECTION
          </Text>
          <Text style={{ alignSelf: 'flex-start', marginLeft: '15%' }}>
            Email
          </Text>
          <TextInput
            text={valueEmail}
            style={styles.input}
            onChangeText={text => this.changeEmail({ text })}
          ></TextInput>
          <Text style={{ alignSelf: 'flex-start', marginLeft: '15%' }}>
            Mot de passe
          </Text>
          <TextInput
            text={valuePwd}
            style={styles.input}
            secureTextEntry={true}
            onChangeText={text => this.changePwd({ text })}
          ></TextInput>
        </View>
        <TouchableHighlight
          style={styles.footer}
          underlayColor={'#21304D'}
          onPress={() => this.connect(valueEmail, valuePwd)}
        >
          <Text>Valider</Text>
        </TouchableHighlight>
        <Text style={{ color: 'darkred' }}>{error}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center'
  },

  headerText: {
    color: 'white',
    fontSize: 36,
    position: 'absolute',
    fontWeight: 'bold',
    top: '15%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  headerSubText: {
    color: 'white',
    fontSize: 16,
    position: 'absolute',
    top: '27%',
    left: '8%',
    zIndex: 3,
    transform: [{ translate: [0, 0, 1] }]
  },

  content: {
    // flex: 1,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

  header: {
    margin: -2,
    height: '45%',
    width: '101%'
  },

  input: {
    width: '70%',
    marginTop: '1%',
    marginBottom: '2%',
    borderRadius: 6,
    borderWidth: 1,
    padding: 10,
    borderColor: '#111E2F'
  },

  footer: {
    margin: 50,
    width: '50%',
    borderRadius: 10,
    borderWidth: 1.5,
    padding: 20,
    borderColor: '#21304D',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
