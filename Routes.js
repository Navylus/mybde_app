import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Auth from './components/AuthPage'
import Cards from './components/SwapMain'
import CreateProject from './components/SwapMain/components/CreateProject'

const Routes = () => (
  <Router>
    <Scene key="root">
      <Scene key="auth" component={Auth} initial={true} hideNavBar={true} />
      <Scene key="cards" component={Cards} hideNavBar={true} />
      <Scene key="createProject" component={CreateProject} hideNavBar={true} />
    </Scene>
  </Router>
)

export default Routes
